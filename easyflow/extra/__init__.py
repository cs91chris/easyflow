from easyflow.core import SEPARATOR
from .builders import Builder

__builders_module__ = "easyflow.extra.builders"
__functions_module__ = "easyflow.extra.functions"


BUILDERS = {
    "ToBase64": __builders_module__ + SEPARATOR + "to_base64",
    "FromBase64": __builders_module__ + SEPARATOR + "from_base64",
    "List2CSV": __builders_module__ + SEPARATOR + "list_to_csv",
    "CSV2List": __builders_module__ + SEPARATOR + "csv_to_list",
    "Dict2XML": __builders_module__ + SEPARATOR + "dict_to_xml",
    "XML2Dict": __builders_module__ + SEPARATOR + "xml_to_dict",
    "JSON2Dict": __builders_module__ + SEPARATOR + "json_to_dict",
    "Dict2JSON": __builders_module__ + SEPARATOR + "dict_to_json",
    "YAML2Dict": __builders_module__ + SEPARATOR + "yaml_to_dict",
    "Dict2YAML": __builders_module__ + SEPARATOR + "dict_to_yaml",
    "XSLT": __builders_module__ + SEPARATOR + "xsl_transformation",
}

FUNCTIONS = {
    "sysdate": __functions_module__ + SEPARATOR + "sysdate",
    "uuid": __functions_module__ + SEPARATOR + "get_uuid",
    "order_by": __functions_module__ + SEPARATOR + "order_by",
    "truncate": __functions_module__ + SEPARATOR + "truncate",
    "reverse": __functions_module__ + SEPARATOR + "reverse",
    "date_format": __functions_module__ + SEPARATOR + "date_format",
}
