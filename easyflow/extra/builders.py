import base64
import csv
import io
import json
from abc import ABC, abstractmethod

import xmltodict
import yaml
from dicttoxml import dicttoxml
from lxml import etree


class Builder(ABC):
    def __init__(self, dh, **kwargs):
        """

        :param dh: DataHandler
        :param kwargs:
        """
        self._dh = dh
        self._params = kwargs

    @abstractmethod
    def build(self, payload):
        """

        :param payload: Payload
        """
        pass

    def getparam(self, key: str):
        """

        :param key:
        :return:
        """
        return self._params.get(key)


class to_base64(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        altchars = self._dh.resolve(payload.get_property("B64_ALTCHARS"), payload)
        buffer = payload.get_buffer()

        if buffer:
            return base64.b64encode(buffer.encode(), altchars=altchars).decode()
        return None


class from_base64(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        altchars = self._dh.resolve(payload.get_property("B64_ALTCHARS"), payload)
        buffer = payload.get_buffer()

        if buffer:
            return base64.b64decode(buffer, altchars=altchars)
        return None


class list_to_csv(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        header = (
            self._dh.resolve(payload.get_property("CSV_HEADER_PRESENT"), payload)
            or True
        )
        quoting = self._dh.resolve(payload.get_property("CSV_QUOTING"), payload) or True
        dialect = (
            self._dh.resolve(payload.get_property("CSV_DIALECT"), payload)
            or "excel-tab"
        )
        delimiter = (
            self._dh.resolve(payload.get_property("CSV_DELIMITER"), payload) or ";"
        )
        qc = self._dh.resolve(payload.get_property("CSV_QUOTE_CHAR"), payload) or '"'

        q = csv.QUOTE_ALL if quoting else csv.QUOTE_NONE
        output = io.StringIO()
        data = payload.get_buffer()

        w = csv.DictWriter(
            output,
            data[0].keys(),
            dialect=dialect,
            delimiter=delimiter,
            quotechar=qc,
            quoting=q,
        )
        if header:
            w.writeheader()
        w.writerows(data)

        return output.getvalue()


class csv_to_list(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        data = payload.get_buffer()
        return [dict(row) for row in csv.DictReader(io.StringIO(data))]


class dict_to_xml(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        tag_upper = (
            self._dh.resolve(payload.get_property("XML_TAG_UPPER"), payload) or True
        )
        root = self._dh.resolve(payload.get_property("XML_TAG_ROOT"), payload) or "root"
        row = self._dh.resolve(payload.get_property("XML_TAG_ROW"), payload) or "row"
        at = (
            self._dh.resolve(payload.get_property("XML_TAG_ATTRIBUTE"), payload) or True
        )
        data = payload.get_buffer()

        return dicttoxml(
            data,
            attr_type=at,
            custom_root=root.upper() if tag_upper else root,
            item_func=lambda x: row.upper() if tag_upper else row,
        ).decode()


class xml_to_dict(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        pn = (
            self._dh.resolve(payload.get_property("XML_PROCESS_NAMESPACES"), payload)
            or False
        )
        data = payload.get_buffer()

        return xmltodict.parse(data, process_namespaces=pn)


class dict_to_json(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        indent = self._dh.resolve(payload.get_property("JSON_INDENT"), payload)
        skip_keys = (
            self._dh.resolve(payload.get_property("JSON_SKIPKEYS"), payload) or False
        )
        separators = self._dh.resolve(
            payload.get_property("JSON_SEPARATORS"), payload
        ) or ((",", ":") if not indent else (", ", ": "))
        data = payload.get_buffer()

        return json.dumps(
            data, indent=indent, separators=separators, skipkeys=skip_keys
        )


class json_to_dict(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        data = payload.get_buffer()
        return json.loads(data)


class dict_to_yaml(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        indent = self._dh.resolve(payload.get_property("YAML_INDENT"), payload)
        allow_unicode = self._dh.resolve(
            payload.get_property("YAML_ALLOW_UNICODE"), payload
        )
        default_flow_style = self._dh.resolve(
            payload.get_property("YAML_DEFAULT_FLOW_STYLE"), payload
        )
        data = payload.get_buffer()

        return yaml.safe_dump(
            data,
            indent=indent,
            allow_unicode=allow_unicode,
            default_flow_style=default_flow_style,
        )


class yaml_to_dict(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        data = payload.get_buffer()

        if not isinstance(data, io.IOBase):
            if not isinstance(data, str):
                data = str(data)
            data = io.StringIO(data)

        return yaml.safe_load(data)


class xsl_transformation(Builder):
    def build(self, payload):
        """

        :param payload: Payload
        :return:
        """
        data = payload.get_buffer()
        xslt_file = self._params.get("file")

        with open(xslt_file) as f:
            tree = etree.parse(
                io.StringIO(data) if not isinstance(data, io.IOBase) else data
            )

            xslt = etree.parse(f)
            value = etree.tostring(tree.xslt(xslt), encoding="UTF-8")
            return dict(xmltodict.parse(value.decode("UTF-8")))
