import uuid as UUID
from datetime import datetime
from operator import itemgetter

from dateutil.parser import parse


def sysdate(fmt="%Y-%m-%d", gmt=False):
    """

    :param fmt:
    :param gmt:
    :return:
    """
    funct = datetime.utcnow if gmt else datetime.now
    return funct().strftime(fmt)


def get_uuid(ver=4, ns=None, name=None):
    """

    :param ver:
    :param ns:
    :param name:
    :return:
    """
    if ver == 1:
        return UUID.uuid1().hex
    if ver == 3:
        return UUID.uuid3(ns or UUID.NAMESPACE_DNS, name).hex
    if ver == 4:
        return UUID.uuid4().hex
    if ver == 5:
        return UUID.uuid5(ns or UUID.NAMESPACE_DNS, name).hex


def order_by(data: list, item, reverse_order=False):
    """

    :param data:
    :param item:
    :param reverse_order:
    :return:
    """
    try:
        return sorted(data, key=itemgetter(item), reverse=reverse_order)
    except KeyError:
        return data


def truncate(data: str, n):
    """

    :param data:
    :param n:
    :return:
    """
    space = " "
    return space.join(data.split()[:n])


def reverse(s: list):
    """

    :param s:
    :return:
    """
    return s[::-1]


def date_format(date: str, fmt="%d %B %Y %I:%M:%S %p"):
    """

    :param date:
    :param fmt:
    :return:
    """
    if date:
        return parse(date).strftime(fmt)
    return None
