from easyflow.core import Context, Payload
from easyflow.exceptions import EFFlowNotFound
from easyflow.flows import BasicFlow
from .iterable import IterableNode
from .node import TemplateNode
from .parallel import ParallelNode


class FlowCallNode(TemplateNode):
    def __init__(self, id_node: str, next_node: str):
        """

        :param id_node:
        :param next_node:
        """
        super().__init__(id_node, next_node)
        self.flow = None

    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        log = ctx.LOG
        id_node = self.get_id()
        payload = ctx.get_payload()

        flow_name = self._data_handler.get("flow")
        resolved = self._data_handler.resolve(flow_name, ctx)
        log.debug(
            "[{}] resolved flow to call: {} -> {}".format(id_node, flow_name, resolved)
        )

        flow_name = resolved

        flowdef = ctx.ENV.get_flow(flow_name)
        if not flowdef:
            ctx.ENV.raiser(
                EFFlowNotFound,
                "[{}] '{}' flow not found in Environ".format(id_node, flow_name),
            )

        # TODO Not sure if this worked well
        if self.flow is None:
            self.flow = BasicFlow(ctx.ENV, flowdef)

        ctx.LOG.debug("[{}] START execution of flow '{}'".format(id_node, flow_name))
        payload = self.flow.run(payload)

        ctx.LOG.debug("[{}] END execution of flow '{}'".format(id_node, flow_name))
        return payload


class IterableFlowCallNode(IterableNode, FlowCallNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        return IterableNode.execute(self, ctx)

    def _iterable_method(self, ctx: Context):
        """

        :param ctx:
        :return:
        """
        return FlowCallNode.execute(self, ctx)


class ParallelFlowCallNode(ParallelNode, FlowCallNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        # noinspection PyCallByClass
        return IterableNode.execute(self, ctx)

    def _parallel_method(self, ctx: Context, item):
        """

        :param ctx:
        :param item:
        :return:
        """
        payload = ctx.get_payload()
        payload.set_buffer(item)
        ctx.set_payload(payload)

        payload = FlowCallNode.execute(self, ctx)
        return payload.get_buffer()
