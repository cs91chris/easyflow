from easyflow.core import Context, Payload
from .node import TemplateNode


class CheckNode(TemplateNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        log = ctx.LOG
        id_node = self.get_id()
        payload = ctx.get_payload()

        condition = self._data_handler.get("condition")
        resolved = self._data_handler.resolve(condition, ctx)

        log.debug(
            "[{}] resolved CONDITION: {} -> {}".format(id_node, condition, resolved)
        )

        data = self._data_handler.get("routes")
        for item in data:
            case = self._data_handler.resolve(item["case"], ctx)
            log.debug("[{}] check case: '{}'".format(id_node, case))

            if resolved == case:
                self._next = self._data_handler.resolve(item["next"], ctx)
                log.debug("[{}] got next node '{}'".format(id_node, self._next))
                break

        return payload
