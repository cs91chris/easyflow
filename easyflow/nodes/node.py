import copy
from abc import ABC, abstractmethod

from easyflow.core import BaseConf, DataHandler


class TemplateNode(ABC):
    def __init__(self, id_node: str, next_node: str):
        """

        :param id_node:
        :param next_node:
        """
        self._id = id_node
        self._next = next_node
        self._conf = None
        self._data_handler = None
        self._private_payload = None

    def init(self, data: DataHandler, conf: BaseConf):
        """

        :param data:
        :param conf:
        """
        self._conf = conf
        self._data_handler = data

    @abstractmethod
    def execute(self, context):
        """

        :param context:
        """
        pass

    def destroy(self):
        """ """
        pass

    def get_next(self) -> str:
        """

        :return:
        """
        return self._next

    def get_id(self) -> str:
        """

        :return:
        """
        return self._id

    def conf_get(self, key: str):
        """

        :param key:
        :return:
        """
        return self._conf.get(key)

    def get_output(self):
        """

        :return:
        """
        return self._private_payload

    def set_output(self, payload):
        """

        :param payload:
        """
        self._private_payload = copy.deepcopy(payload)
