from abc import abstractmethod
from collections import Iterable
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool as ThreadPool

from easyflow.core import Context, Payload
from .node import TemplateNode


class ParallelNode(TemplateNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return: payload
        """
        log = ctx.LOG
        id = self.get_id()
        payload = ctx.get_payload()
        buffer = payload.get_buffer()

        if isinstance(buffer, Iterable):
            nprocs = min(len(buffer), cpu_count())

            pool = ThreadPool(nprocs)
            output = pool.starmap(self._parallel_method, [(ctx, i) for i in buffer])
        else:
            output = self._parallel_method(ctx, buffer)

        payload.set_buffer(output)
        return payload

    @abstractmethod
    def _parallel_method(self, ctx: Context, item):
        """

        :param ctx:
        :param item:
        :return: buffer
        """
        pass
