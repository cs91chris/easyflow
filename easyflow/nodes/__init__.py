from easyflow.core import SEPARATOR
from .check import CheckNode
from .data import DataNode
from .flowcall import FlowCallNode, IterableFlowCallNode, ParallelFlowCallNode
from .iterable import IterableNode
from .node import TemplateNode
from .parallel import ParallelNode
from .subflowcall import (
    IterableSubFlowCallNode,
    ParallelSubFlowCallNode,
    SubFlowCallNode,
)

__nodes_module__ = "easyflow.nodes"

NODES = {
    "DataNode": __nodes_module__ + SEPARATOR + "DataNode",
    "CheckNode": __nodes_module__ + SEPARATOR + "CheckNode",
    "FlowCallNode": __nodes_module__ + SEPARATOR + "FlowCallNode",
    "SubFlowCallNode": __nodes_module__ + SEPARATOR + "SubFlowCallNode",
    "IterableFlowCallNode": __nodes_module__ + SEPARATOR + "IterableFlowCallNode",
    "IterableSubFlowCallNode": __nodes_module__ + SEPARATOR + "IterableSubFlowCallNode",
}
