from abc import abstractmethod
from collections import Iterable

from easyflow.core import Context, Payload
from easyflow.exceptions import EFRuntimeError
from .node import TemplateNode


class IterableNode(TemplateNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        log = ctx.LOG
        payload = ctx.get_payload()

        accumulate = self.conf_get("accumulate")
        exit_on_error = self.conf_get("exitOnError")
        buffer = payload.get_buffer()

        output = []

        if isinstance(buffer, Iterable):
            for i in buffer:
                try:
                    ctx.get_payload().set_buffer(i)
                    data = self._iterable_method(ctx)
                    if accumulate is True:
                        output.append(data.get_buffer())
                    else:
                        output = data.get_buffer()
                except EFRuntimeError as exc:
                    if exit_on_error is True:
                        raise EFRuntimeError
                    else:
                        log.error(exc)
        else:
            data = self._iterable_method(ctx)
            output = data.get_buffer()

        payload.set_buffer(output)
        return payload

    @abstractmethod
    def _iterable_method(self, ctx: Context):
        """

        :param ctx:
        :return:
        """
        pass
