from easyflow.core import Context, Payload
from easyflow.exceptions import EFFlowNotFound
from easyflow.flows import BasicFlow
from . import IterableNode, ParallelNode, TemplateNode


class SubFlowCallNode(TemplateNode):
    def __init__(self, id_node: str, next_node: str):
        """

        :param id_node:
        :param next_node:
        """
        super().__init__(id_node, next_node)
        self._subflow = None

    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        log = ctx.LOG
        id_node = self.get_id()
        payload = ctx.get_payload()

        subflow_name = self._data_handler.conf_get("subflow")
        resolved = self._data_handler.resolve(subflow_name, ctx)
        log.debug(
            "[{}] resolved subflow to call: {} -> {}".format(
                id_node, subflow_name, resolved
            )
        )

        subflow_name = resolved

        subflowdef = ctx.get_subflow(subflow_name)
        if not subflowdef:
            ctx.ENV.raiser(
                EFFlowNotFound,
                "[{}] '{}' subflow not found".format(id_node, subflow_name),
            )

        # Not sure if this worked well
        if self._subflow is None:
            self._subflow = BasicFlow(ctx.ENV, subflowdef)

        ctx.LOG.debug(
            "[{}] START execution of subflow '{}'".format(id_node, subflow_name)
        )
        payload = self._subflow.run(payload)

        ctx.LOG.debug(
            "[{}] END execution of subflow '{}'".format(id_node, subflow_name)
        )
        return payload


class IterableSubFlowCallNode(IterableNode, SubFlowCallNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        return IterableNode.execute(self, ctx)

    def _iterable_method(self, ctx: Context):
        """

        :param ctx:
        :return:
        """
        return SubFlowCallNode.execute(self, ctx)


class ParallelSubFlowCallNode(ParallelNode, SubFlowCallNode):
    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        return ParallelNode.execute(self, ctx)

    def _parallel_method(self, ctx: Context, item):
        """

        :param ctx:
        :param item:
        :return:
        """
        payload = ctx.get_payload()
        payload.set_buffer(item)
        ctx.set_payload(payload)

        payload = SubFlowCallNode.execute(self, ctx)
        return payload.get_buffer()
