import copy

from easyflow.core import Context, Payload
from .node import TemplateNode


class DataNode(TemplateNode):
    def set_props(self, ctx: Context):
        """

        :param ctx:
        """
        data = self._data_handler.get("props")
        if data is not None:
            for k, v in data.items():
                expr = self._data_handler.resolve(v, ctx)
                ctx.get_payload().set_property(k, expr)
                ctx.LOG.debug("[{}] has set the property '{}'".format(self.get_id(), k))

    def set_buffer(self, ctx: Context):
        """

        :param ctx:
        """
        data = self._data_handler.get("buffer")
        if data is not None:
            ctx.get_payload().set_buffer(self._data_handler.resolve(data, ctx))
            ctx.LOG.debug("[{}] has updated buffer".format(self.get_id()))

    def build_output(self, ctx: Context):
        """

        :param ctx:
        :return:
        """
        self._data_handler.build_output(ctx)

        if self.conf_get("keepout") is True:
            self._private_payload = copy.deepcopy(ctx.get_payload())
            ctx.LOG.debug("[{}] saved its output's payload".format(self.get_id()))

    def execute(self, ctx: Context) -> Payload:
        """

        :param ctx:
        :return:
        """
        self.set_props(ctx)
        self.set_buffer(ctx)
        self.build_output(ctx)

        return ctx.get_payload()
