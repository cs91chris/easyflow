from easyflow.core import SEPARATOR
from .defaultphs import ConfPh, ContextPh, EvalPh, Placeholder
from .objectpath import CSSSelectorPh, JsonPathPh, XPathPh

__placeholders_module__ = "easyflow.placeholders"

PLACEHOLDERS = {
    "CONF": __placeholders_module__ + SEPARATOR + "ConfPh",
    "CTX": __placeholders_module__ + SEPARATOR + "ContextPh",
    "JSON": __placeholders_module__ + SEPARATOR + "JsonPathPh",
    "CSS": __placeholders_module__ + SEPARATOR + "CSSSelectorPh",
    "XPATH": __placeholders_module__ + SEPARATOR + "XPathPh",
    "EVAL": __placeholders_module__ + SEPARATOR + "EvalPh",
}
