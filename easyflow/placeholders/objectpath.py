import json
from json import JSONDecodeError
from xml.etree import ElementTree

from bs4 import BeautifulSoup
from lxml import etree, html
from objectpath import ExecutionError, ProgrammingError, Tree

from easyflow.core import SEPARATOR
from easyflow.exceptions import EFRuntimeError
from .config import BUFFER_KEY, PROPS_KEY
from .defaultphs import Placeholder


def __resolve_placeholder__(context, expression):
    """

    :param context:
    :param expression:
    :return:
    """
    payload = context.get_payload()
    data = expression.split(SEPARATOR)
    action = data[0]

    if len(data) > 2:
        key = data[1]
        expr = data[2]
    elif len(data) == 2:
        key = None
        expr = data[1]
    else:
        key = None
        expr = data[0]

    if action == PROPS_KEY:
        input_data = payload.get_property(key)
    elif action == BUFFER_KEY:
        if key:
            payload = context.get_node(key).get_output()
            input_data = payload.get_buffer() if payload else None
        else:
            input_data = payload.get_buffer()
    else:
        input_data = None
    return input_data, expr


class JsonPathPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        input_data, expr = __resolve_placeholder__(context, self._expr)

        try:
            if not isinstance(input_data, dict):
                try:
                    input_data = json.loads(input_data)
                except JSONDecodeError:
                    context.ENV.raiser(
                        EFRuntimeError, "Malformed JSON input:\n%s" % input_data
                    )
                except TypeError:
                    context.ENV.raiser(
                        EFRuntimeError,
                        "Invalid type of data input allowed str, bytes, bytearray: "
                        "type -> '%s'" % type(input_data),
                    )

            tree = Tree(input_data)
            return tree.execute(expr)
        except (ProgrammingError, ExecutionError):
            context.ENV.raiser(
                EFRuntimeError, "Unable to evaluate placeholder value '%s'" % expr
            )


class XPathPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        output = []
        input_data, expr = __resolve_placeholder__(context, self._expr)

        try:
            tree = html.fromstring(input_data)
            for i in tree.xpath(expr):
                output.append(ElementTree.tostring(i).decode())
        except (etree.XPathEvalError, etree.XPathSyntaxError):
            context.ENV.raiser(
                EFRuntimeError, "Unable to evaluate placeholder value '%s'" % expr
            )
        return output


class CSSSelectorPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        input_data, expr = __resolve_placeholder__(context, self._expr)

        try:
            xml = BeautifulSoup(input_data, "html5lib")
            return xml.select(expr)
        except (SyntaxError, TypeError):
            context.ENV.raiser(
                EFRuntimeError, "Unable to evaluate placeholder value '%s'" % expr
            )
