import re


PH_FORMAT = "<<%s>>"
PH_REGEX = re.compile("<<([^<>]*)>>")

PROPS_KEY = "PROPS"
BUFFER_KEY = "BUFFER"
