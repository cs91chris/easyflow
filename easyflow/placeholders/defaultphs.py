from abc import ABC, abstractmethod

from simpleeval import EvalWithCompoundTypes as SimpleEval

from easyflow.core import SEPARATOR
from easyflow.exceptions import EFRuntimeError
from .config import BUFFER_KEY, PROPS_KEY


class Placeholder(ABC):
    def __init__(self, value: str):
        """

        :param value:
        """
        self._expr = value

    @abstractmethod
    def resolve(self, context):
        """

        :param context:
        """
        pass


class ConfPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        return context.Environ.SYSCONF.conf_get(self._expr)


class EvalPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        functions = context.ENV.MAPPINGS.get("FUNCTIONS")
        properties = context.get_payload().get_properties()

        s = SimpleEval(functions=functions, names=properties)
        return s.eval(self._expr)


class ContextPh(Placeholder):
    def resolve(self, context):
        """

        :param context:
        :return:
        """
        data = self._expr.split(SEPARATOR)
        action = data[0]
        key = data[1] if len(data) > 1 else None
        payload = context.get_payload()

        if action == PROPS_KEY:
            return payload.get_property(key)

        if action == BUFFER_KEY:
            if key:
                payload = context.get_node(key).get_output()
                return payload.get_buffer() if payload else None
            return payload.get_buffer()
        else:
            context.ENV.raiser(
                EFRuntimeError, "Unable to parse placeholder action '%s'" % action
            )
