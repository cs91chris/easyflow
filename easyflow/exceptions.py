class EFConfError(Exception):
    """Configuration error: malformed yaml files"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass


class EFRuntimeError(RuntimeError):
    """Runtime error: error in flow execution"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass


class EFInstantiateError(EFConfError):
    """Runtime error: error in flow execution"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass


class EFExtensionNotFound(EFRuntimeError):
    """Runtime error: error in flow execution"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass


class EFFlowNotFound(EFRuntimeError):
    """Runtime error: error in flow execution"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass


class EFBadRouting(EFRuntimeError):
    """Runtime error: error in flow execution"""

    def __init__(self, *args, **kwargs):
        """

        :param args:
        :param kwargs:
        """
        pass
