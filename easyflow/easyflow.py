from .core import Environ, Payload
from .exceptions import EFConfError
from .extra import BUILDERS, FUNCTIONS
from .flows import BasicFlow
from .nodes import NODES
from .placeholders import PLACEHOLDERS
from .validators import VALIDATORS

__mappings__ = {
    "NODES": NODES,
    "VALIDATORS": VALIDATORS,
    "PLACEHOLDERS": PLACEHOLDERS,
    "BUILDERS": BUILDERS,
    "FUNCTIONS": FUNCTIONS,
}


class EasyFlow:
    def __init__(self):
        """ """
        self._env = None

    def bootstrap(self, sysconf=None, logconf=None, mappings=None, flows=None):
        """

        :param sysconf:
        :param logconf:
        :param mappings:
        :param flows:
        :return:
        """
        if logconf is not None:
            import logging.config

            logging.config.dictConfig(logconf)

        self._env = Environ(sysconf or {}, __mappings__)

        if mappings is not None:
            for name, mapping in mappings.items():
                for k, v in (mapping if isinstance(mapping, dict) else {}).items():
                    self._env.add_mapping(name, k, v)
                    self._env.LOG.info(
                        "loaded mapping '{}' -> {}:{}".format(name, k, v)
                    )

        if flows is not None:
            for f in flows:
                self._env.add_flow(f)
                self._env.LOG.info("loaded flow '{}'".format(f["name"]))

    def startflow(self, name: str, props=None, data=None):
        """

        :param name:
        :param props:
        :param data:
        :return:
        """
        flowdef = self._env.get_flow(name)

        if flowdef is not None:
            try:
                flow = BasicFlow(self._env, flowdef)
                self._env.LOG.info("instantiated flow '{}'".format(name))
            except EFConfError as exc:
                self._env.LOG.error(exc)
            else:
                self._env.LOG.info("Flow '{}' STARTED".format(name))
                self._env.LOG.debug(
                    self._env.dumpdata(
                        "Dump Flow '{}' definition".format(name), flowdef
                    )
                )
                return flow.run(Payload(props, data))
        else:
            self._env.LOG.warning("Flow '{}' not found!".format(name))

        return None
