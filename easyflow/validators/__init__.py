from easyflow.core import SEPARATOR
from .validators import (
    BasicFlowSchema,
    CheckNodeSchema,
    ConfSchema,
    DataNodeSchema,
    DataSchema,
    FlowCallNodeSchema,
    IterableFlowCallNodeSchema,
    IterableSchema,
    IterableSubFlowCallNodeSchema,
    NodeSchema,
    SubFlowCallNodeSchema,
)

__validators_module__ = "easyflow.validators"

VALIDATORS = {
    "BasicFlow": __validators_module__ + SEPARATOR + "BasicFlowSchema",
    "DataNode": __validators_module__ + SEPARATOR + "DataNodeSchema",
    "CheckNode": __validators_module__ + SEPARATOR + "CheckNodeSchema",
    "FlowCallNode": __validators_module__ + SEPARATOR + "FlowCallNodeSchema",
    "SubFlowCallNode": __validators_module__ + SEPARATOR + "SubFlowCallNodeSchema",
    "IterableFlowCallNode": __validators_module__
    + SEPARATOR
    + "IterableFlowCallNodeSchema",
    "IterableSubFlowCallNode": __validators_module__
    + SEPARATOR
    + "IterableSubFlowCallNodeSchema",
}
