from colander import (
    Any,
    Boolean,
    Mapping,
    MappingSchema,
    SchemaNode,
    SequenceSchema,
    String,
    drop,
    instantiate,
)


class ConfSchema(MappingSchema):
    input = SchemaNode(String(), missing=drop)
    onexception = SchemaNode(String(), missing=drop)


class DataSchema(MappingSchema):
    pass


class NodeSchema(MappingSchema):
    id = SchemaNode(String())
    type = SchemaNode(String())
    next = SchemaNode(String(), missing=None)
    conf = ConfSchema(Mapping(unknown="preserve"), missing={})
    data = DataSchema(Mapping(unknown="preserve"), missing={})


class Nodes(SequenceSchema):
    node = NodeSchema()


class BasicFlowSchema(MappingSchema):
    name = SchemaNode(String())
    start = SchemaNode(String())
    nodes = Nodes(missing=[])
    loglevel = SchemaNode(String(), missing=drop)

    @instantiate(missing=[])
    class subflows(SequenceSchema):
        sub = SchemaNode(Mapping(unknown="preserve"))


class DataNodeSchema(DataSchema):
    @instantiate(missing={})
    class conf(ConfSchema):
        keepout = SchemaNode(Boolean(), missing=drop)

    @instantiate(missing={})
    class data(DataSchema):
        buffer = Any()
        outbuilder = SchemaNode(String(), missing=drop)
        props = SchemaNode(Mapping(unknown="preserve"), missing=drop)
        outparams = SchemaNode(Mapping(unknown="preserve"), missing=drop)


class CheckNodeSchema(NodeSchema):
    @instantiate(missing={})
    class data(DataSchema):
        condition = SchemaNode(String())

        @instantiate()
        class routes(SequenceSchema):
            @instantiate()
            class Route(MappingSchema):
                case = SchemaNode(String())
                next = SchemaNode(String())


class IterableSchema(NodeSchema):
    @instantiate(missing={})
    class conf(ConfSchema):
        keepout = SchemaNode(Boolean(), missing=drop)
        accumulate = SchemaNode(Boolean(), missing=False)
        exitOnError = SchemaNode(Boolean(), missing=True)


class FlowCallNodeSchema(NodeSchema):
    @instantiate(missing={})
    class data(DataSchema):
        flow = SchemaNode(String())


class SubFlowCallNodeSchema(NodeSchema):
    @instantiate(missing={})
    class data(DataSchema):
        subflow = SchemaNode(String())


class IterableFlowCallNodeSchema(FlowCallNodeSchema, IterableSchema):
    pass


class IterableSubFlowCallNodeSchema(SubFlowCallNodeSchema, IterableSchema):
    pass
