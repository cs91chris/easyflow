from abc import ABC, abstractmethod

from easyflow.core import Context, Environ, Payload


class IFlow(ABC):
    def __init__(self, env: Environ, flowdef: dict):
        """

        :param env:
        :param flowdef:
        """
        self._name = flowdef["name"]
        self._start_node = flowdef["start"]
        self._log_level = flowdef.get("loglevel")
        self.LOG = env.setup_logger(self._name, self._log_level)

        self._context = Context(env, self.LOG)

        self._init_nodes(flowdef["nodes"])
        self._init_subflows(flowdef["subflows"])

    def get_name(self) -> str:
        """

        :return:
        """
        return self._name

    @abstractmethod
    def _init_nodes(self, nodes: list):
        """

        :param nodes:
        """
        pass

    @abstractmethod
    def _init_subflows(self, subflows: list):
        """

        :param subflows:
        """
        pass

    @abstractmethod
    def run(self, payload) -> Payload:
        """

        :param payload:
        """
        pass

    @abstractmethod
    def __del__(self):
        """ """
        pass
