from easyflow.core import BaseConf, DataHandler, Payload
from easyflow.exceptions import EFRuntimeError
from .flow import IFlow


class BasicFlow(IFlow):
    def _init_nodes(self, nodes: list):
        """

        :param nodes:
        """
        for n in nodes:
            node_id = n["id"]
            node_class = self._context.ENV.get_node_type(n["type"])
            self._context.add_node(node_id, node_class(node_id, n["next"]))
            self.LOG.debug("[{}] added node '{}'".format(self._name, node_id))

            node = self._context.get_node(node_id)
            node.init(DataHandler(n["data"]), BaseConf(n["conf"]))
            self.LOG.debug("[{}] initialized node '{}'".format(self._name, node_id))

    def _init_subflows(self, subflows: list):
        """

        :param subflows:
        """
        for sf in subflows:
            name = sf["name"]
            self._context.add_subflow(name, sf)
            self.LOG.debug("[{}] added subflow '{}'".format(self._name, name))

    def run(self, payload) -> Payload:
        """

        :param payload:
        :return:
        """
        item = self._start_node
        # noinspection PyPep8Naming
        CTX = self._context
        # noinspection PyPep8Naming
        ENV = CTX.ENV

        CTX.set_payload(payload)

        while item:
            try:
                node = CTX.get_node(item)
                id_node = node.get_id()

                node_input = node.conf_get("input")
                if node_input:
                    n = CTX.get_node(node_input)
                    CTX.set_payload(n.get_output())
                else:
                    CTX.set_payload(payload)

                self.LOG.debug(
                    ENV.dumpdata(
                        "[{}] INPUT payload at node '{}'".format(
                            self.get_name(), id_node
                        ),
                        CTX.get_payload(),
                        self._log_level,
                    )
                )
                self.LOG.debug(
                    "[{}] START execution of node '{}'".format(self.get_name(), id_node)
                )

                payload = node.execute(CTX)

            except EFRuntimeError as exc:
                self.LOG.error(exc)
                item = node.conf_get("onexception")
            else:
                item = node.get_next()

            self.LOG.debug(
                "[{}] END execution of node '{}'".format(self.get_name(), id_node)
            )
            self.LOG.debug(
                ENV.dumpdata(
                    "[{}] OUTPUT payload at node '{}'".format(self.get_name(), id_node),
                    CTX.get_payload(),
                    self._log_level,
                )
            )

        return CTX.get_payload()

    def __del__(self):
        """ """
        for _, node in self._context.get_nodes().items():
            if node:
                node.destroy()
