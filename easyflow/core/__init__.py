SEPARATOR = "::"

from .context import Context, Payload
from .datahandler import DataHandler
from .environ import Environ
from .extension import EFExtension
from .sysconf import BaseConf, Dumpable, MapConf
