from abc import ABC, abstractmethod


class EFExtension(ABC):
    @abstractmethod
    def init_env(self, env, **kwargs):
        """

        :param env:
        :param kwargs:
        """
        pass

    @abstractmethod
    def destroy(self):
        """ """
        pass

    def __del__(self):
        """ """
        self.destroy()
