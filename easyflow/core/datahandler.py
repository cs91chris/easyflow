from easyflow.exceptions import EFRuntimeError
from easyflow.placeholders.config import PH_FORMAT, PH_REGEX
from . import Context, SEPARATOR


class DataHandler:
    def __init__(self, conf: dict):
        """

        :param conf:
        """
        self._conf = conf

    def get(self, key):
        """

        :param key:
        :return:
        """
        return self._conf.get(key)

    def build_output(self, ctx: Context):
        """

        :param ctx:
        :return:
        """
        payload = ctx.get_payload()

        builder = self.get("outbuilder")
        params = self.get("buildparams")

        if builder:
            try:
                builder_class = ctx.ENV.get_builder(builder)
            except KeyError as exc:
                ctx.ENV.raiser(EFRuntimeError, exc)

            output = builder_class(self, **(params or {}))
            payload.set_buffer(output.build(payload))

    @staticmethod
    def resolve(expr, ctx: Context):
        """

        :param expr:
        :param ctx:
        :return:
        """
        if isinstance(expr, str) or isinstance(expr, bytes):
            while True:
                resolved = []
                phs = PH_REGEX.findall(expr)

                if len(phs) == 0:
                    break

                for p in phs:
                    resolved.append(
                        (PH_FORMAT % p, str(DataHandler._eval_placeholder(p, ctx)))
                    )

                for r in resolved:
                    expr = expr.replace(*r)

        return expr

    @staticmethod
    def _eval_placeholder(p, ctx):
        """

        :param p:
        :param ctx:
        :return:
        """
        expression = p
        data = expression.split(SEPARATOR)

        if len(data) > 1:
            try:
                ph = ctx.ENV.get_placeholder(data[0])
            except KeyError as exc:
                ctx.LOG.warn("placeholder {} not found!".format(exc))

            expression = ph(SEPARATOR.join(data[1:])).resolve(ctx)
            ctx.LOG.debug(
                "Got expression from placeholder {}: {}".format(ph, expression)
            )

        return expression
