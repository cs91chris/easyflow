import logging
import traceback
from functools import update_wrapper
from logging import handlers

from colander import Invalid

from easyflow.exceptions import EFConfError, EFExtensionNotFound
from easyflow.validators import BasicFlowSchema
from .extension import EFExtension
from .sysconf import BaseConf, MapConf


def setupmethod(funct):
    """

    :param funct:
    :return:
    """

    def wrapper(self, *args, **kwargs):
        """

        :param self: object reference
        :param args:
        :param kwargs:
        :return:
        """
        if self._setup_ended:
            raise AssertionError(
                "A setup function was called after the first flow was requested. "
                "Method '{}' must be called at application startup.".format(
                    funct.__name__
                )
            )
        return funct(self, *args, **kwargs)

    return update_wrapper(wrapper, funct)


class Environ:
    def __init__(self, sysconf: dict, mappings: dict):
        """

        :param sysconf:
        :param mappings:
        """
        self._setup_ended = False

        self._ext = {}
        self.FLOWS = {}

        self.SYSCONF = BaseConf(sysconf)
        self.MAPPINGS = MapConf(mappings)

        self.SYSCONF.set("ENV", self.SYSCONF.get("ENV") or "DEV")
        self.SYSCONF.set("LOG_LEVEL", self.SYSCONF.get("LOG_LEVEL") or "INFO")

        self.LOG = self.setup_logger()

        self.LOG.info(
            "Registered configuration for environment: '{}'".format(
                self.SYSCONF.get("ENV")
            )
        )

    @setupmethod
    def _flowschema_validator(self, flowdef: dict, schema=BasicFlowSchema):
        """

        :param flowdef:
        :param schema:
        :return:
        """
        flowdef = schema().deserialize(flowdef)

        for node in flowdef["nodes"]:
            validators = self.MAPPINGS.get("VALIDATORS")

            if node["type"] in validators:
                v = validators[node["type"]]()
                # noinspection PyUnusedLocal
                node = v.deserialize(node)
            else:
                mess = "[{}]: schema validator not found for node '{}'"
                self.LOG.warning(mess.format(flowdef["name"], node["id"]))

        return flowdef

    @setupmethod
    def add_extension(self, key: str, ext: EFExtension, **kwargs):
        """

        :param key:
        :param ext:
        """
        if not issubclass(ext.__class__, EFExtension):
            self.raiser(
                EFConfError, "Invalid extension class: '%s'" % ext.__class__.__name__
            )
        if self._ext.get(key) is not None:
            self.raiser(
                EFConfError,
                "A Conflict name occurred: '%s' extension is already registered. "
                "You should change your extension's name" % key,
            )
        ext.init_env(self, **kwargs)
        self._ext[key] = ext

    @setupmethod
    def add_flow(self, flowdef: dict):
        """

        :param flowdef:
        """
        try:
            flowdef = self._flowschema_validator(flowdef)

            for subflow in flowdef["subflows"]:
                # noinspection PyUnusedLocal
                subflow = self._flowschema_validator(subflow)
        except Invalid as exc:
            self.raiser(EFConfError, exc)
        else:
            self.FLOWS[flowdef["name"]] = flowdef

    @setupmethod
    def add_mapping(self, custom: str, name: str, definition: str):
        """

        :param custom:
        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map(custom, name, definition)

    @setupmethod
    def add_node_type(self, name: str, definition: str):
        """

        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map("NODES", name, definition)

    @setupmethod
    def add_validator(self, name: str, definition: str):
        """

        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map("VALIDATORS", name, definition)

    @setupmethod
    def add_placeholder(self, name: str, definition: str):
        """

        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map("PLACEHOLDERS", name, definition)

    @setupmethod
    def add_builder(self, name: str, definition: str):
        """

        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map("BUILDERS", name, definition)

    @setupmethod
    def add_function(self, name: str, definition: str):
        """

        :param name:
        :param definition:
        """
        self.MAPPINGS.set_map("FUNCTIONS", name, definition)

    def get_extension(self, ext: str):
        """

        :param ext:
        :return:
        """
        ext = self._ext.get(ext)
        if not ext:
            self.raiser(EFExtensionNotFound, "EFExtension: '%s' not found".format(ext))
        return ext

    def get_flow(self, name: str):
        """

        :param name:
        :return:
        """
        self._setup_ended = True
        return self.FLOWS.get(name)

    def get_mapping(self, custom: str, name: str):
        """

        :param custom:
        :param name:
        :return:
        """
        return (self.MAPPINGS.get(custom) or {}).get(name)

    def get_node_type(self, name: str):
        """

        :param name:
        :return:
        """
        return (self.MAPPINGS.get("NODES") or {}).get(name)

    def get_validator(self, name: str):
        """

        :param name:
        :return:
        """
        return (self.MAPPINGS.get("VALIDATORS") or {}).get(name)

    def get_placeholder(self, name: str):
        """

        :param name:
        :return:
        """
        return (self.MAPPINGS.get("PLACEHOLDERS") or {}).get(name)

    def get_builder(self, name: str):
        """

        :param name:
        :return:
        """
        return (self.MAPPINGS.get("BUILDERS") or {}).get(name)

    def get_function(self, name: str):
        """

        :param name:
        :return:
        """
        return (self.MAPPINGS.get("FUNCTIONS") or {}).get(name)

    def raiser(self, exc, *args):
        """

        :param exc:
        :return:
        """
        self.LOG.error(traceback.format_exc())
        raise exc(*args) from None

    def dumpdata(self, txt: str, data, level=None):
        """

        :param txt:
        :param data:
        :param level:
        """
        level = level or self.SYSCONF.get("LOG", {}).get("level")
        if level == "DEBUG":
            try:
                data = data.dump()
            except AttributeError:
                pass

            return "{}\n{}".format(txt, data)
        return ""

    def setup_logger(self, name=None, level=None):
        """

        :param name:
        :param level:
        :return:
        """
        log = logging.getLogger(self.SYSCONF.get("ENV"))

        if name:
            log = logging.getLogger("{}.{}".format(self.SYSCONF.get("ENV"), name))
            file_dir = self.SYSCONF.get("LOG", {}).get("path") or "."
            fh = handlers.RotatingFileHandler(
                filename="{}/{}.log".format(file_dir, name)
            )

            log_format = self.SYSCONF.get("LOG", {}).get("format")

            if log_format:
                fh.setFormatter(logging.Formatter(log_format))
            if level:
                log.setLevel(level or logging.INFO)

            log.addHandler(fh)
        return log
