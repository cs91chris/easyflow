import importlib

import yaml

from easyflow.exceptions import EFConfError, EFInstantiateError
from . import SEPARATOR


class Dumpable:
    def dump(self) -> str:
        """ """
        raise NotImplementedError


class BaseConf(Dumpable):
    def __init__(self, conf: dict):
        """

        :param conf:
        """
        self._conf = conf

    def get(self, key, safe=False):
        """

        :param key:
        :param safe:
        :return:
        """
        try:
            return self._conf[key]
        except (TypeError, KeyError):
            if safe is True:
                raise EFConfError("key '{}' not found".format(key))
            return None

    def set(self, key: str, value):
        """

        :param key:
        :param value:
        """
        self._conf[key] = value

    def dump(self) -> str:
        """

        :return:
        """
        return yaml.dump(
            self._conf, indent=4, allow_unicode=True, default_flow_style=False
        )


class MapConf(BaseConf):
    def __init__(self, conf: dict):
        """

        :param conf:
        """
        super().__init__(conf)

        for k, v in self._conf.items():
            if not k.startswith("_") and v is not None:
                for x, w in v.items():
                    v[x] = MapConf.import_from_module(w)

    def set_map(self, mapper: str, key: str, value: str):
        """

        :param mapper:
        :param key:
        :param value:
        """
        try:
            attr = getattr(self, mapper)
            attr[key] = MapConf.import_from_module(value)
        except AttributeError:
            setattr(self, mapper, {})
            attr = getattr(self, mapper)
            attr[key] = MapConf.import_from_module(value)

    @staticmethod
    def import_from_module(name: str):
        """

        :param name:
        :return:
        """
        try:
            mod, attr = name.split(SEPARATOR)
            module = importlib.import_module(mod)
            return getattr(module, attr)
        except ModuleNotFoundError:
            mess = "Module '{}' not found"
            raise EFInstantiateError(mess.format(mod)) from None
        except AttributeError:
            mess = "Attribute '{}' not found in module '{}'"
            raise EFInstantiateError(mess.format(attr, mod)) from None
