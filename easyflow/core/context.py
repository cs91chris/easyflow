import yaml

from easyflow.exceptions import EFBadRouting, EFConfError
from .sysconf import Dumpable


class Payload(Dumpable):
    def __init__(self, props=None, buf=None):
        """

        :param props:
        :param buf:
        """
        self._buffer = buf
        self._properties = props or {}

    def get_properties(self) -> dict:
        """

        :return:
        """
        return self._properties

    def get_property(self, k: str):
        """

        :param k:
        :return:
        """
        return self._properties.get(k)

    def set_properties(self, props: dict):
        """

        :param props:
        """
        self._properties = props

    def set_property(self, k: str, v):
        """

        :param k:
        :param v:
        """
        self._properties[k] = v

    def set_buffer(self, value):
        """

        :param value:
        """
        self._buffer = value

    def get_buffer(self):
        """

        :return:
        """
        return self._buffer

    def dump(self) -> str:
        """

        :return:
        """
        return "\nPROPERTIES:\n{}\nBUFFER:\n{}\n".format(
            yaml.safe_dump(
                self._properties, indent=4, allow_unicode=True, default_flow_style=False
            ),
            self._buffer,
        )


class Context:
    def __init__(self, env, log=None):
        """

        :param env:
        :param log:
        """
        self._payload = Payload()
        self._nodes = {None: None}
        self._subflows = {}
        self.ENV = env
        self.LOG = log or self.ENV.LOG

        if self.ENV is None:
            raise EFConfError(
                """
                  Context can not be instantiate without an 'Environ' object. Create it first!
            """
            )

    def get_payload(self) -> Payload:
        """

        :return:
        """
        return self._payload

    def set_payload(self, payload: Payload):
        """

        :param payload:
        """
        self._payload = payload or Payload()

    def get_nodes(self) -> dict:
        """

        :return:
        """
        return self._nodes

    def get_subflows(self) -> dict:
        """

        :return:
        """
        return self._subflows

    def add_node(self, name: str, node):
        """

        :param name:
        :param node:
        """
        if self._nodes.get(name) is not None:
            self.ENV.raiser(
                EFConfError,
                "Node with id '{}' already exists. Check flow configuration".format(
                    name
                ),
            )

        self._nodes[name] = node

    def get_node(self, name: str):
        """

        :param name:
        :return:
        """
        node = self._nodes.get(name)
        if not node:
            self.ENV.raiser(EFBadRouting, "Node with id '{}' not found in flow")
        return node

    def add_subflow(self, name: str, subflow):
        """

        :param name:
        :param subflow:
        """
        self._subflows[name] = subflow

    def get_subflow(self, name: str):
        """

        :param name:
        :return:
        """
        return self._subflows.get(name)

    def get_output(self, node: str):
        """

        :param node:
        """
        self.get_node(node).get_output()
