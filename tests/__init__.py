import os
import yaml


BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

FILES = {
    "LOG": os.path.join(BASE_DIR, "config", "logging.yaml"),
    "TEST_FLOW": os.path.join(BASE_DIR, "config", "flows", "test.yaml"),
    "CALLER_FLOW": os.path.join(BASE_DIR, "config", "flows", "call.yaml"),
}

sys_conf = {
    "ENV": "DEV",
    "LOG": {
        "path": "log",
        "level": "DEBUG",
        "format": "[%(asctime)s][%(levelname)s][%(module)s:%(lineno)d]: %(message)s",
    },
}


def load_from_yaml(file: str):
    with open(file) as file:
        return yaml.safe_load(file)


def get_log():
    return load_from_yaml(FILES["LOG"])


def get_flows():
    return [
        {
            "name": "test_buffer",
            "start": "first",
            "nodes": [
                {
                    "id": "first",
                    "type": "DataNode",
                    "next": None,
                    "conf": {},
                    "data": {"buffer": "buffer", "props": {"A": "A"}},
                }
            ],
        },
    ]
