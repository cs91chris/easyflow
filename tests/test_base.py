import pytest

from easyflow import EasyFlow
from . import get_log, sys_conf, get_flows


@pytest.fixture
def app():
    _app = EasyFlow()
    flows = get_flows()
    log_config = get_log()

    _app.bootstrap(sysconf=sys_conf, logconf=log_config, flows=flows)

    return _app


def test_base(app):
    output = app.startflow("test_buffer")
    assert output.get_buffer() == "buffer"
    assert output.get_property("A") == "A"
